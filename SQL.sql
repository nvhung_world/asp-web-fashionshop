-- MySQL dump 10.13  Distrib 5.7.12, for Win64 (x86_64)
--
-- Host: localhost    Database: fashionshop
-- ------------------------------------------------------
-- Server version	5.7.16-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


DROP TABLE IF EXISTS `taikhoan`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `taikhoan` (
  `MaTK` int(11) NOT NULL AUTO_INCREMENT,
  `TaiKhoan` varchar(30) CHARACTER SET utf8 NOT NULL,
  `MatKhau` varchar(30) CHARACTER SET utf8 NOT NULL,
  `Quyen` enum('Khách Hàng','Nhân Viên','Quản Trị Viên','Ngưng Hoạt Động') COLLATE utf8_unicode_ci NOT NULL DEFAULT 'Khách Hàng',
  `NgayLap` date NOT NULL,
  `MaCap2` varchar(30) CHARACTER SET utf8 DEFAULT NULL,
  `Email` varchar(50) CHARACTER SET utf8 DEFAULT NULL,
  `LastLogin` date DEFAULT NULL,
  PRIMARY KEY (`MaTK`),
  UNIQUE KEY `TaiKhoan` (`TaiKhoan`)
) ENGINE=InnoDB AUTO_INCREMENT=22 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

LOCK TABLES `taikhoan` WRITE;
/*!40000 ALTER TABLE `taikhoan` DISABLE KEYS */;
INSERT INTO `taikhoan` VALUES (0,'NoName','noname','Khách Hàng','2016-04-12',NULL,NULL,NULL),(8,'admin','admin','Quản Trị Viên','2016-12-03','','','2016-12-12'),(15,'AnhHung','hungit','Khách Hàng','2016-03-12',NULL,NULL,NULL),(20,'TuanTuTi','tuan','Khách Hàng','2016-03-12',NULL,NULL,NULL),(21,'abcd','abcd','Khách Hàng','2016-12-07',NULL,NULL,NULL);
/*!40000 ALTER TABLE `taikhoan` ENABLE KEYS */;
UNLOCK TABLES;

DROP TABLE IF EXISTS `thongtintk`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `thongtintk` (
  `MaTK` int(11) NOT NULL,
  `Ho` varchar(15) CHARACTER SET utf8 DEFAULT NULL,
  `Ten` varchar(20) CHARACTER SET utf8 DEFAULT NULL,
  `Avatar` varchar(100) CHARACTER SET utf8 DEFAULT NULL,
  `GioiTinh` enum('Nam','Nữ','Không Dõ') COLLATE utf8_unicode_ci NOT NULL DEFAULT 'Không Dõ',
  `NgaySinh` date DEFAULT NULL,
  `SoCMT` varchar(15) CHARACTER SET utf8 DEFAULT NULL,
  `Email` varchar(50) CHARACTER SET utf8 DEFAULT NULL,
  `SoDT` varchar(15) CHARACTER SET utf8 DEFAULT NULL,
  `DiaChi` varchar(50) CHARACTER SET utf8 DEFAULT NULL,
  PRIMARY KEY (`MaTK`),
  CONSTRAINT `fk_TT_MaTK` FOREIGN KEY (`MaTK`) REFERENCES `taikhoan` (`MaTK`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

LOCK TABLES `thongtintk` WRITE;
/*!40000 ALTER TABLE `thongtintk` DISABLE KEYS */;
INSERT INTO `thongtintk` VALUES (0,'No','Name',NULL,'Không Dõ',NULL,NULL,NULL,NULL,NULL),(8,'Nguyễn','Hưng',NULL,'Không Dõ',NULL,'','','',''),(15,'Nguyễn ','Hưng',NULL,'Nam',NULL,NULL,NULL,NULL,NULL),(20,'nguyễn','Tuấn',NULL,'Không Dõ',NULL,NULL,NULL,NULL,NULL),(21,'Nguyen','a',NULL,'Nam',NULL,NULL,NULL,NULL,NULL);
/*!40000 ALTER TABLE `thongtintk` ENABLE KEYS */;
UNLOCK TABLES;

DROP TABLE IF EXISTS `mathang`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `mathang` (
  `MaMH` int(11) NOT NULL AUTO_INCREMENT,
  `TenMH` varchar(30) CHARACTER SET utf8 NOT NULL,
  `Loai` varchar(20) CHARACTER SET utf8 NOT NULL DEFAULT 'Không Dõ',
  `TonKho` int(11) NOT NULL DEFAULT '0',
  `GiaBan` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`MaMH`)
) ENGINE=InnoDB AUTO_INCREMENT=22 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

LOCK TABLES `mathang` WRITE;
/*!40000 ALTER TABLE `mathang` DISABLE KEYS */;
INSERT INTO `mathang` VALUES (1,'Áo khoác Phao','Áo Khoác',10,200000),(2,'Váy Zen','Váy',20,300000),(3,'Váy Ngắn','Váy',30,200000),(4,'Áo khoác thể thao','Áo Khoác',10,200000),(5,'Áo khoác có mũ','Áo Khoác',10,200000),(6,'Áo khoác da','Áo Khoác',10,200000),(7,'Áo thun','Áo',10,200000),(8,'Áo thun không cổ','Áo',10,200000),(9,'Áo polo','Áo',10,200000),(10,'Áo thun dài tay','Áo',10,200000),(11,'Áo len','Áo',10,200000),(12,'Áo sơ mi nam','Áo',10,200000),(13,'Sơ mi dài tay','Áo',10,200000),(14,'Sơ mi ngắn tay','Áo',10,200000),(15,'Quần kaki','Quần',10,200000),(16,'Quần jean','Quần',10,200000),(17,'Quần tây','Quần',10,200000),(18,'Quần lót','Quần',10,200000),(19,'Quần boxer','Quần',10,200000),(20,'Quần shorts','Quần',10,200000),(21,'Vest','Vest',10,200000);
/*!40000 ALTER TABLE `mathang` ENABLE KEYS */;
UNLOCK TABLES;

DROP TABLE IF EXISTS `thongtinmh`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `thongtinmh` (
  `MaMH` int(11) NOT NULL,
  `Avatar` varchar(100) CHARACTER SET utf8 DEFAULT NULL,
  `ThuongHieu` varchar(50) CHARACTER SET utf8 DEFAULT NULL,
  `DoiTuong` varchar(20) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'All',
  `Nam` int(11) DEFAULT NULL,
  `LuotXem` int(11) NOT NULL DEFAULT '0',
  `MoTaNgan` varchar(50) CHARACTER SET utf8 DEFAULT NULL,
  PRIMARY KEY (`MaMH`),
  CONSTRAINT `fk_TTMH_MaMH` FOREIGN KEY (`MaMH`) REFERENCES `mathang` (`MaMH`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

LOCK TABLES `thongtinmh` WRITE;
/*!40000 ALTER TABLE `thongtinmh` DISABLE KEYS */;
INSERT INTO `thongtinmh` VALUES (1,'fashion\\a(1).jpg','May 10','Nữ',2016,76,'x'),(2,'fashion\\a(2).jpg','May 10','Nữ',2016,56,'ư'),(3,'fashion\\a(3).jpg','May 10','Nam',2016,25,'e'),(4,'fashion\\a(4).jpg','May 10','Nam',2016,11,'r'),(5,'fashion\\a(5).jpg','Việt Tiến ','Nam',2016,12,'t'),(6,'fashion\\a(6).jpg','Việt Tiến','Nữ',2015,11,'y'),(7,'fashion\\a(7).jpg','Việt Tiến','Nữ',2015,9,'u'),(8,'fashion\\a(8).jpg','Việt Tiến','Nữ',2015,12,'i'),(9,'fashion\\a(9).jpg','BLUE EXCHANGE','Nữ',2015,13,'o'),(10,'fashion\\a(10).jpg','BLUE EXCHANGE','Bé Trai',2015,10,'p'),(11,'fashion\\a(11).jpg','BLUE EXCHANGE','Bé Trai',2013,11,'a'),(12,'fashion\\a(12).jpg','BLUE EXCHANGE','Bé Trai',2013,12,'s'),(13,'fashion\\a(13).jpg','Novelty','Bé Trai',2013,14,'d'),(14,'fashion\\a(14).jpg','Novelty','Bé Gái',2013,15,'f'),(15,'fashion\\a(15).jpg','Novelty','Bé Gái',2013,15,'g'),(16,'fashion\\a(16).jpg','PT2000','Bé Gái',2014,17,'h'),(17,'fashion\\a(17).jpg','PT2000','Nam',2014,19,'j'),(18,'fashion\\a(18).jpg','Ninomaxx','Nam',2014,22,'k'),(19,'fashion\\a(1).jpg','Ninomaxx','Nam',2014,20,'l'),(20,'fashion\\a(12).jpg','Ninomaxx','Nữ',2016,22,'x'),(21,'fashion\\a(13).jpg','Ninomaxx','Nữ',2014,21,'z');
/*!40000 ALTER TABLE `thongtinmh` ENABLE KEYS */;
UNLOCK TABLES;


DROP TABLE IF EXISTS `donhang`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `donhang` (
  `MaDH` int(11) NOT NULL AUTO_INCREMENT,
  `MaTK` int(11) NOT NULL,
  `NgayBan` date NOT NULL,
  `TrangThai` enum('Giỏ Hàng','Đang Chuyển','Đã Thanh Toán') CHARACTER SET utf8 NOT NULL DEFAULT 'Giỏ Hàng',
  `NoiGiao` varchar(100) CHARACTER SET utf8 DEFAULT NULL,
  PRIMARY KEY (`MaDH`),
  KEY `fk_DH_MaTK` (`MaTK`),
  CONSTRAINT `fk_DH_MaTK` FOREIGN KEY (`MaTK`) REFERENCES `taikhoan` (`MaTK`) ON DELETE NO ACTION ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;


LOCK TABLES `donhang` WRITE;
/*!40000 ALTER TABLE `donhang` DISABLE KEYS */;
INSERT INTO `donhang` VALUES (1,20,'2016-12-04','Giỏ Hàng','                                                                                                    '),(3,20,'2016-12-04','Đang Chuyển','                                                                                    '),(4,20,'2016-12-04','Đang Chuyển','                                                                                    '),(5,20,'2016-12-04','Đang Chuyển','                                                                                    '),(6,20,'2016-12-04','Giỏ Hàng',NULL),(11,0,'2016-12-05','Đã Thanh Toán',NULL),(12,0,'2016-12-05','Đã Thanh Toán',NULL),(13,0,'2016-12-05','Đã Thanh Toán',NULL),(14,0,'2016-12-05','Đã Thanh Toán',NULL),(15,0,'2016-12-05','Đã Thanh Toán',NULL),(16,0,'2016-12-05','Đã Thanh Toán',NULL),(17,8,'2016-12-10','Giỏ Hàng',NULL);
/*!40000 ALTER TABLE `donhang` ENABLE KEYS */;
UNLOCK TABLES;


DROP TABLE IF EXISTS `chitietdh`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `chitietdh` (
  `IDCTDH` int(11) NOT NULL AUTO_INCREMENT,
  `MaDH` int(11) NOT NULL,
  `MaMH` int(11) NOT NULL,
  `SoLuong` int(11) NOT NULL DEFAULT '0',
  `MaKM` int(11) NOT NULL,
  PRIMARY KEY (`IDCTDH`),
  KEY `fk_CTDH_MaDH` (`MaDH`),
  KEY `fk_CTDH_MaMH` (`MaMH`),
  KEY `fk_chitietdh_KhuyenMai1_idx` (`MaKM`),
  CONSTRAINT `fk_CTDH_MaDH` FOREIGN KEY (`MaDH`) REFERENCES `donhang` (`MaDH`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_CTDH_MaMH` FOREIGN KEY (`MaMH`) REFERENCES `mathang` (`MaMH`) ON DELETE NO ACTION ON UPDATE CASCADE,
  CONSTRAINT `fk_chitietdh_KhuyenMai1` FOREIGN KEY (`MaKM`) REFERENCES `khuyenmai` (`MaKM`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=32 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;


LOCK TABLES `chitietdh` WRITE;
/*!40000 ALTER TABLE `chitietdh` DISABLE KEYS */;
INSERT INTO `chitietdh` VALUES (9,1,3,1,0),(10,6,18,1,0),(23,11,1,1,0),(24,12,2,1,0),(25,13,4,1,0),(26,14,5,1,0),(27,15,2,13,0),(28,16,3,3,0),(29,6,7,1,0),(30,6,2,3,0),(31,1,1,2,0);
/*!40000 ALTER TABLE `chitietdh` ENABLE KEYS */;
UNLOCK TABLES;


DROP TABLE IF EXISTS `khuyenmai`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `khuyenmai` (
  `MaKM` int(11) NOT NULL AUTO_INCREMENT,
  `TenKM` varchar(45) NOT NULL,
  `NgayBD` date DEFAULT NULL,
  `NgayKT` date DEFAULT NULL,
  `MucGiamGia` int(11) DEFAULT '0',
  `MoTa` varchar(100) NOT NULL,
  PRIMARY KEY (`MaKM`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;


LOCK TABLES `khuyenmai` WRITE;
/*!40000 ALTER TABLE `khuyenmai` DISABLE KEYS */;
/*!40000 ALTER TABLE `khuyenmai` ENABLE KEYS */;
UNLOCK TABLES;

DROP TABLE IF EXISTS `chitietkm`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `chitietkm` (
  `MaCTKM` int(11) NOT NULL,
  `MaKM` int(11) NOT NULL,
  `MaMH` int(11) NOT NULL,
  PRIMARY KEY (`MaCTKM`),
  KEY `fk_ChiTietKM_KhuyenMai1_idx` (`MaKM`),
  KEY `fk_ChiTietKM_mathang1_idx` (`MaMH`),
  CONSTRAINT `fk_ChiTietKM_KhuyenMai1` FOREIGN KEY (`MaKM`) REFERENCES `khuyenmai` (`MaKM`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_ChiTietKM_mathang1` FOREIGN KEY (`MaMH`) REFERENCES `mathang` (`MaMH`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

LOCK TABLES `chitietkm` WRITE;
/*!40000 ALTER TABLE `chitietkm` DISABLE KEYS */;
/*!40000 ALTER TABLE `chitietkm` ENABLE KEYS */;
UNLOCK TABLES;

DROP TABLE IF EXISTS `nhacungcap`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `nhacungcap` (
  `MaNCC` int(11) NOT NULL AUTO_INCREMENT,
  `TenNCC` varchar(30) CHARACTER SET utf8 NOT NULL,
  `SoDT` varchar(15) CHARACTER SET utf8 DEFAULT NULL,
  `Email` varchar(50) CHARACTER SET utf8 DEFAULT NULL,
  `DiaChi` varchar(50) CHARACTER SET utf8 DEFAULT NULL,
  `MoTa` varchar(100) CHARACTER SET utf8 DEFAULT NULL,
  PRIMARY KEY (`MaNCC`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

LOCK TABLES `nhacungcap` WRITE;
/*!40000 ALTER TABLE `nhacungcap` DISABLE KEYS */;
/*!40000 ALTER TABLE `nhacungcap` ENABLE KEYS */;
UNLOCK TABLES;

DROP TABLE IF EXISTS `phieunhap`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `phieunhap` (
  `MaPN` int(11) NOT NULL AUTO_INCREMENT,
  `MaNCC` int(11) NOT NULL,
  `MaMH` int(11) NOT NULL,
  `SoLuong` int(11) NOT NULL DEFAULT '0',
  `DonGia` int(11) NOT NULL DEFAULT '0',
  `NgayNhap` date NOT NULL,
  PRIMARY KEY (`MaPN`),
  KEY `fk_MH_MaMH` (`MaMH`),
  KEY `fk_PN_MaNCC` (`MaNCC`),
  CONSTRAINT `fk_MH_MaMH` FOREIGN KEY (`MaMH`) REFERENCES `mathang` (`MaMH`) ON DELETE NO ACTION ON UPDATE CASCADE,
  CONSTRAINT `fk_PN_MaNCC` FOREIGN KEY (`MaNCC`) REFERENCES `nhacungcap` (`MaNCC`) ON DELETE NO ACTION ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

LOCK TABLES `phieunhap` WRITE;
/*!40000 ALTER TABLE `phieunhap` DISABLE KEYS */;
/*!40000 ALTER TABLE `phieunhap` ENABLE KEYS */;
UNLOCK TABLES;



/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2016-12-12 22:01:50
