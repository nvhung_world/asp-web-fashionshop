﻿<%@ Page Title="" Language="C#" MasterPageFile="~/form/PageMaster.Master" AutoEventWireup="true" CodeBehind="ShoppingCart.aspx.cs" Inherits="FashionShop.form.ShoppingCart" %>

<%@ Register Src="~/form/GIoHangMH.ascx" TagPrefix="fashionshop" TagName="GioHangMH" %>
<%@ Register Src="~/form/GioHang.ascx" TagPrefix="fashionshop" TagName="GH" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="row" style="width:90%; margin: 250px auto auto auto; min-height: 500px">
        <div class="panel col-md-4">
            <asp:PlaceHolder ID="PlaceHolderDH" runat="server"></asp:PlaceHolder>
        </div>
        <div class="col-md-8">
            <asp:PlaceHolder ID="PlaceHolderMH" runat="server"></asp:PlaceHolder>
        </div>
    </div>
</asp:Content>
