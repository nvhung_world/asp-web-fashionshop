﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="GioHang.ascx.cs" Inherits="FashionShop.form.GioiHang" %>
<style>
    .NoiGiao{
        width: 100%;
        margin: auto auto auto auto;
        resize: vertical;
    }
</style>
<div id="Begin" class="panel" runat="server" style="width:100%;padding-top: 10px">
    <table style="width: 100%" class="table-condensed">
        <tr>
            <td>
                Tổng Sản phẩm
            </td>
            <td>
                <asp:Label ID="controlTSP" runat="server" Text="Label"></asp:Label>
            </td>
        </tr>
        <tr>
            <td>
                Tổng Tiền
            </td>
            <td>
                <asp:Label ID="controlT" runat="server" Text="Label"></asp:Label>
            </td>
        </tr>
        <tr>
            <td>
                Ngày Mua
            </td>
            <td>
                <asp:Label ID="controlNM" runat="server" Text="Label"></asp:Label>
            </td>
        </tr>
        <tr>
            <td>
                Trạng Thái
            </td>
            <td>
                <asp:Label ID="controlTT" runat="server" Text="Label"></asp:Label>
            </td>
        </tr>
        <tr>
            <td colspan="2">
                Nơi Giao
            </td>
        </tr>
        <tr>
            <td colspan="2">
                <asp:TextBox ID="controlNG" runat="server" TextMode="MultiLine" Rows="3" CssClass="NoiGiao" ></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td>
                <asp:Button ID="Button_1" runat="server" Text="Button" OnClick="XacNhan_Click"/>
            </td>
            <td>
                <asp:Button ID="Button_2" runat="server" Text="Button" OnClick="Huy_Click"/>
            </td>
        </tr>
        <tr>
            <td colspan="2">
                <asp:HyperLink ID="HyperLink" runat="server" CssClass="btn btn-primary">Xem chi Tiết</asp:HyperLink>
            </td>
        </tr>
    </table>
</div>
