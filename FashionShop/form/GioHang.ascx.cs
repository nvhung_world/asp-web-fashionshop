﻿using System;
using System.Data;
using FashionShop.AllClass;

namespace FashionShop.form
{
    public partial class GioiHang : System.Web.UI.UserControl
    {
        Connect conn;
        public bool Select = false;
        public int MaDH;
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                conn = new Connect();
                conn.Open();
                Result res = conn.GetData("SELECT NgayBan, NoiGiao, TrangThai, sum(SoLuong) as 'So Luong', sum(SoLuong * GiaBan - (GiaBan * case when MucGiamGia is null then 0 else MucGiamGia end)) as 'tong tien' FROM donhang left join chitietdh on donhang.MaDH = chitietdh.MaDH left join mathang on chitietdh.MaMH = mathang.MaMH  left join khuyenmai on chitietdh.MaKM = khuyenmai.MaKM where donhang.MaDH = "+ MaDH +" group by donhang.MaDH");
                if(res.complete)
                {
                    DataTable dt = (DataTable)res.res;
                    if(dt.Rows.Count >0)
                    {
                        DataRow r = dt.Rows[0];
                        controlNM.Text = Convert.ToDateTime( r[0].ToString()).ToShortDateString();
                        controlNG.Text = r[1].ToString();
                        controlTT.Text = r[2].ToString();
                        controlTSP.Text = r[3].ToString();
                        controlT.Text = r[4].ToString() + " VNĐ";
                        if(controlTT.Text == "Giỏ Hàng")
                        {
                            Button_1.Text = "Xác nhận đơn hàng";
                            Button_2.Text = "Hủy Đơn Hàng";
                        }
                        else
                            Button_1.Visible = Button_2.Visible = false;

                        if (Select)
                        {
                            Begin.Attributes.Add("style", "background-color:#F4A460");
                            HyperLink.Visible = false;
                        }
                        else
                            Begin.Attributes.Add("style", "background-color:#C0C0C0");
                        HyperLink.NavigateUrl = "~/form/ShoppingCart.aspx?MaDH=" + MaDH;
                    }
                }
            }
            catch(Exception)
            {
            }
            finally
            {
                conn.Close();
            }
        }
        //
        protected void XacNhan_Click(object sender, EventArgs e)
        {
            try
            {
                Connect conn = new Connect();
                conn.Open();
                Result res = conn.ExecuteNonQuery("UPDATE donhang SET TrangThai = 'Đang Chuyển', NoiGiao = '" + controlNG.Text + "' WHERE MaDH = " + MaDH);
                if (res.complete)
                {
                    LayDonHang(conn);
                    Response.Redirect(Request.Url.PathAndQuery);
                }
            }catch(Exception)
            {
            }
            finally
            {
                conn.Close();
            }
        }
        protected void LayDonHang(Connect conn)
        {
            Result res = conn.GetData("SELECT MaDH, TrangThai, NgayBan, NoiGiao FROM fashionshop.donhang where MaTK = " + Session["MaTK"].ToString());
            if (res.complete)
            {
                DataTable dt = (DataTable)res.res;
                if (dt.Rows.Count > 0)
                {
                    Session["MaDH"] = dt.Rows[0][0].ToString();
                    Session["TrangThai"] = dt.Rows[0][1].ToString();
                    Session["NgayBan"] = dt.Rows[0][1].ToString();
                    Session["NoiGiao"] = dt.Rows[0][1].ToString();
                }
                else
                {
                    res = conn.ExecuteNonQuery("INSERT INTO donhang(MaTK, NgayBan)VALUES(" + Session["MaTK"].ToString() + ", '" + GetNow() + "');");
                    LayDonHang(conn);
                }
            }
        }
        protected string GetNow()
        {
            return "" + DateTime.Now.Year + "-" + DateTime.Now.Month + "-" + DateTime.Now.Day + "";
        }
        protected void Huy_Click(object sender, EventArgs e)
        {
            try
            {
                Connect conn = new Connect();
                conn.Open();
                Result res = conn.ExecuteNonQuery("DELETE FROM donhang WHERE MaDH = " + MaDH);
                if (res.complete)
                {
                    Response.Redirect(Request.Url.PathAndQuery);
                }
            }
            catch (Exception)
            {
            }
            finally
            {
                conn.Close();
            }
        }
    }
}