﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace FashionShop.form
{
    public partial class IconMH : System.Web.UI.UserControl
    {
        public string TenMH = "", //Tên Mặt Hàng
            GiaTien = "",        //Giá Tiền
            KhuyenMai = "",      // Khuyễn Mại
            ThuongHieu = "",     //Thương Hiệu
            LuotXem = "",        //Lượt Xem
            Avatar = "",         // Link ảnh đại diện
            MaMH = "";           // Mã Mặt Hàng, không hiển thị
        protected void Page_Load(object sender, EventArgs e)
        {
            TextTenMH.Text = TenMH;
            TextGiaTien.Text = GiaTien + " đ";
            if(KhuyenMai != "")
                TextKhuyenMai.Text = KhuyenMai + " %";
            else
                TextKhuyenMai.Text = "0 %";
            TextThuongHieu.Text = ThuongHieu;
            TextLoutXem.Text = LuotXem + " lượt xem";
            TextAvatar.ImageUrl ="../" + Avatar;
            HyperLink.NavigateUrl = "Detailed.aspx?MaMH=" + MaMH;
        }
        
    }
}