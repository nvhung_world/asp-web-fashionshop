﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="IconMH.ascx.cs" Inherits="FashionShop.form.IconMH" %>
<div>
    <div class="thumbnail">
        <asp:Image ID="TextAvatar" runat="server"/>
    </div>
    <div style="
    width: 100%;
    text-align: center;
    font-size: 16px;
    color: #2f2f2f;
    margin-bottom: 10px;
    color: #333333;" >
        <asp:Label ID="TextTenMH" runat="server"></asp:Label>
    </div>
    <div style="
     width: 100%;
     text-align: center;
     font-weight: 500;
     margin-bottom: 15px;
     font-size: 20px;
     line-height: 24px;">
        <asp:Label ID="TextGiaTien" ForeColor="Orange" runat="server"></asp:Label> 
        (-<asp:Label ID="TextKhuyenMai" ForeColor="Red" runat="server"></asp:Label>)
    </div>
    <div style="text-align:center;font-size:15px;">
        <asp:Label ID="TextThuongHieu" CssClass="label label-primary" runat="server"></asp:Label>
        <asp:Label ID="TextLoutXem" CssClass="label label-default" runat="server"></asp:Label>
    </div>
    <div style="
         width: 100%;
         color:blue;
         text-align: center;
         font-weight: 500;
         margin-bottom: 15px;
         font-size: 15px;
         line-height: 24px;">
        <asp:HyperLink ID="HyperLink" runat="server">Chi Tiết</asp:HyperLink>
    </div>
</div>
