﻿using System;
using System.Data;
using FashionShop.AllClass;

namespace FashionShop.form
{
    public partial class Register : System.Web.UI.Page
    {
        Connect conn;
        object oldURL;
        protected void Page_Load(object sender, EventArgs e)
        {
            oldURL = Session["oldURL"];
            if (Session["User"] != null)
                Session.Abandon();
        }

        protected void dangky_Click(object sender, EventArgs e)
        {
            try
            {
                conn = new Connect();
                conn.Open();
                string Ho = TextHo.Text,
                    Ten = TextTen.Text,
                    Email = TextEmail.Text,
                    NS = TextNS.SelectedDate.Year + "-" + TextNS.SelectedDate.Month + "-" + TextNS.SelectedDate.Day,
                    Acc = TextAcc.Text,
                    Pass = TextPass.Text,
                    GT= TextGT.Text;
                Result res = conn.ExecuteNonQuery("INSERT INTO taikhoan(TaiKhoan,MatKhau,Quyen,NgayLap)VALUES(N'"+Acc+"',N'"+Pass+"', 'Khách Hàng', now()); INSERT INTO thongtintk(matk, ho, ten, gioitinh, Email, NgaySinh)VALUES(LAST_INSERT_ID(), '"+Ho+"', '"+Ten+"', '"+GT+ "', '" + Email + "', '" + NS + "')");
                if (res.complete)
                {
                    res = conn.GetData("select taikhoan.MaTK, Quyen, concat(Ho, ' ', Ten) from taikhoan, thongtintk where taikhoan.MaTK = thongtintk.MaTK and upper(TaiKhoan) = upper(N'" + Acc + "') and MatKhau = N'" + Pass + "'");
                    if (res.complete)
                    {
                        DataTable dt = (DataTable)res.res;
                        if (dt.Rows.Count > 0)
                        {
                            Session["User"] = Acc;
                            Session["MaTK"] = dt.Rows[0][0].ToString();
                            Session["Quyen"] = dt.Rows[0][1].ToString();
                            Session["HoTen"] = dt.Rows[0][2].ToString();
                            TextDebug.Text = "";
                            res = conn.ExecuteNonQuery("UPDATE taikhoan SET LastLogin = '" + GetNow() + "' WHERE MaTK = " + dt.Rows[0][0].ToString());
                            LayDonHang();
                            if (oldURL != null)
                                Response.Redirect(oldURL.ToString());
                            else
                                Response.Redirect("../form/Home.aspx");
                        }
                        else
                        {
                            TextDebug.Text = "Sai thông tin đăng nhập";
                        }
                    }
                }
                else
                {
                    TextDebug.Text = "Lỗi: " + res.Error;
                }
            }
            catch (Exception)
            { }
            finally
            {
                conn.Close();
            }
        }
        protected void LayDonHang()
        {
            Result res = conn.GetData("SELECT MaDH, TrangThai, NgayBan, NoiGiao FROM fashionshop.donhang where MaTK = " + Session["MaTK"].ToString());
            if (res.complete)
            {
                DataTable dt = (DataTable)res.res;
                if (dt.Rows.Count > 0)
                {
                    Session["MaDH"] = dt.Rows[0][0].ToString();
                    Session["TrangThai"] = dt.Rows[0][1].ToString();
                    Session["NgayBan"] = dt.Rows[0][1].ToString();
                    Session["NoiGiao"] = dt.Rows[0][1].ToString();
                }
                else
                {
                    res = conn.ExecuteNonQuery("INSERT INTO donhang(MaTK, NgayBan)VALUES(" + Session["MaTK"].ToString() + ", '" + GetNow() + "');");
                    LayDonHang();
                }
            }
        }
        protected string GetNow()
        {
            return "" + DateTime.Now.Year + "-" + DateTime.Now.Month + "-" + DateTime.Now.Day + "";
        }
    }
}