﻿<%@ Page Title="" Language="C#" MasterPageFile="~/form/PageMaster.Master" AutoEventWireup="true" CodeBehind="Detailed.aspx.cs" Inherits="FashionShop.form.Detailed" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div style="margin:250px auto auto 50px" class="row">
        <div class="col-md-5">
            <asp:Image ID="Avatar" CssClass="thumbnail" runat="server" />
        </div>
        <div class="col-md-6 panel">
            <div class =" panel-heading" style="color:orange; text-align:center; font-size: 20px; font-weight:bolder">
                <asp:Label ID="TTSP" runat="server" Text="Thông Tin Sản Phẩm"></asp:Label>
            </div>
            <div class="panel-body">
                <table  style="width:100%" class="table table-hover" >
                    <tr>
                        <td>
                            Tên Mặt Hàng
                        </td>
                        <td>
                            <asp:Label ID="TenMH" runat="server" Text="N/A"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Giá Bán
                        </td>
                        <td>
                            <asp:Label ID="GiaTien" runat="server" Text="N/A"></asp:Label>
                            <asp:Label ID="KhuyenMai" runat="server" Text=""></asp:Label>
                            <asp:Label ID="ThanhTien" runat="server" Text="N/A"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Loại
                        </td>
                        <td>
                            <asp:Label ID="Loai" runat="server" Text="N/A"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Đối Tượng
                        </td>
                        <td>
                            <asp:Label ID="DoiTuong" runat="server" Text="N/A"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Năm Sản Xuất
                        </td>
                        <td>
                            <asp:Label ID="Nam" runat="server" Text="N/A"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Thương Hiệu
                        </td>
                        <td>
                            <asp:Label ID="ThuongHieu" runat="server" Text="N/A"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Lượt Xem
                        </td>
                        <td>
                            <asp:Label ID="LuotXem" runat="server" Text="N/A"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="3">
                            Mô Tả
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <asp:Label ID="MoTa" runat="server" Text="N/A"></asp:Label>
                        </td>
                    </tr>
                </table>
            </div>
        </div>
    </div>
    <div style="text-align:center">
            <asp:Button ID="MuaTru" CssClass="btn btn-danger" runat="server" Text="-" OnClick="MuaTru_Click"/>
            <asp:Label ID="MuaLabel" CssClass="text-info" runat="server" Text=""></asp:Label>
            <asp:Button ID="MuaCong" CssClass="btn btn-success"  runat="server" Text="+" OnClick="MuaCong_Click"/>
            <asp:Label ID="MuaLabel2" runat="server" Text="Mặt hàng Trong Giỏ"></asp:Label>
            <asp:HyperLink NavigateUrl="~/form/Login.aspx" ID="MuaDN" CssClass="btn btn-primary" runat="server">Đăng Nhập</asp:HyperLink>
        </div>
</asp:Content>
