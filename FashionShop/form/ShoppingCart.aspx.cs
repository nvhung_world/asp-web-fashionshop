﻿using System;
using System.Data;
using FashionShop.AllClass;

namespace FashionShop.form
{
    public partial class ShoppingCart : System.Web.UI.Page
    {
        Connect conn;
        protected void Page_Load(object sender, EventArgs e)
        {
            Session["OldURL"] = Request.Url.PathAndQuery;
            if (Session["MaTK"] != null)
            {
                try
                {
                    conn = new Connect();
                    conn.Open();
                    Result res = conn.GetData("select MaDH, TrangThai  from DonHang where MaTK = " + Session["MaTK"].ToString() + " ORDER BY NgayBan desc");
                    if(res.complete)
                    {
                        string MaDH = null, TrangThai = "Giỏ Hàng";
                        if (Request.QueryString["MaDH"] != null)
                            MaDH = Request.QueryString["MaDH"];
                        DataTable dt = (DataTable)res.res;
                        foreach(DataRow r in dt.Rows)
                        {
                            if (MaDH == null)
                            {
                                MaDH = r[0].ToString();
                                TrangThai = r[1].ToString();
                            }
                            GioiHang GH = (GioiHang)LoadControl("~/form/GioHang.ascx");
                            GH.MaDH = Convert.ToInt32(r[0]);
                            if (GH.MaDH.ToString() == MaDH)
                            {
                                GH.Select = true;
                                TrangThai = r[1].ToString();
                            }
                            PlaceHolderDH.Controls.Add(GH);
                        }

                        res = conn.GetData("SELECT IDCTDH FROM chitietdh where MaDH =" + MaDH);
                        if(res.complete)
                        {
                            dt = (DataTable)res.res;
                            foreach (DataRow r in dt.Rows)
                            {
                                GIoHangMH GH = (GIoHangMH)LoadControl("~/form/GIoHangMH.ascx");
                                GH.IDCTDH = Convert.ToInt32(r[0]);
                                GH.Huy = TrangThai != "Giỏ Hàng";
                                PlaceHolderMH.Controls.Add(GH);
                            }
                        }
                    }
                }
                catch (Exception)
                {

                }
                finally
                {
                    conn.Close();
                }
            }
            else
                Response.Redirect("../form/Login.aspx");
        }
    }
}