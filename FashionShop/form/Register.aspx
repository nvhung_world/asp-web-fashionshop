﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Register.aspx.cs" Inherits="FashionShop.form.Register" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Register</title>
    <link href="../css/bootstrap.css" rel="stylesheet" />
    <link href="../css/Register.css" rel="stylesheet" />
    <style>
        .Red{
            color:red;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">
        <div class="panel panel-danger" style="width:40%;margin: 30px auto auto auto">
            <div>
                <a href="home.aspx">
                    <asp:Image ID="Image1" CssClass="icon-bar center-block" Width="140px" Height="140px" ImageUrl="../pic/ab1.png" runat="server" />
                </a> 
            </div>
             <table style="width: 900%; margin-top:10px" class="table table-hover">
                 <tr>
                    <td style="color:blue;font-size:15px; text-align: right">
                        Họ
                    </td>
                    <td style="text-align: left">
                        <asp:TextBox ID="TextHo" CssClass="form-control"  Width="100%" runat="server"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ErrorMessage="Họ Không được bỏ trống" ControlToValidate="TextHo" Text="(*)" CssClass="Red"></asp:RequiredFieldValidator>
                     </td>
                </tr>
                 <tr>
                    <td style="color:blue;font-size:15px; text-align: right">
                        Tên
                    </td>
                    <td style="text-align: left">
                        <asp:TextBox ID="TextTen" CssClass="form-control"  Width="100%" runat="server"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ErrorMessage="Tên Không được bỏ trống" ControlToValidate="TextTen" Text="(*)" CssClass="Red"></asp:RequiredFieldValidator>
                    </td>
                </tr>
                 <tr>
                    <td style="color:blue;font-size:15px; text-align: right">
                        Giới Tính
                    </td>
                    <td>
                        <asp:DropDownList ID="TextGT" runat="server">
                            <asp:ListItem Text="Không Dõ" Value="Không Dõ" Selected="True"></asp:ListItem>
                            <asp:ListItem Text="Nam" Value="Nam"></asp:ListItem>
                            <asp:ListItem Text="Nữ" Value="Nữ"></asp:ListItem>
                        </asp:DropDownList>
                    </td>
                </tr>
                <tr>
                    <td style="color:blue;font-size:15px; text-align: right">
                        Email
                    </td>
                    <td>
                        <asp:TextBox ID="TextEmail" CssClass="form-control"  Width="100%" runat="server"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage="Email Không được bỏ trống" ControlToValidate="TextEmail" Text="(*)" CssClass="Red"></asp:RequiredFieldValidator>
                        <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ErrorMessage="Sai Email" ControlToValidate="TextEmail" ValidationExpression="^([\w\.\-]+)@([\w\-]+)((\.(\w){2,3})+)$" Text="(*)" CssClass="Red"></asp:RegularExpressionValidator>
                    </td>
                </tr>
                <tr>
                    <td style="color:blue;font-size:15px; text-align: right">
                        Ngày sinh
                    </td>
                    <td>
                        <asp:Calendar ID="TextNS" runat="server"></asp:Calendar>
                    </td>
                </tr>
                <tr>
                    <td style="color:blue;font-size:15px; text-align: right">
                        Tên Đăng Nhập
                    </td>
                    <td>
                        <asp:TextBox ID="TextAcc" CssClass="form-control"  MaxLength="30"  Width="100%" runat="server"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" ErrorMessage="Tên đăng nhập Không được bỏ trống" ControlToValidate="TextAcc" Text="(*)" CssClass="Red"></asp:RequiredFieldValidator>                        
                    </td>
                </tr>
                <tr>
                    <td style="color:blue;font-size:15px; text-align: right">
                        Mật Khẩu
                    </td>
                    <td>
                        <asp:TextBox ID="TextPass" CssClass="form-control"  MaxLength="30"  Width="100%"  runat="server"  TextMode="Password"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator6" runat="server" ErrorMessage="Mật khẩu Không được bỏ trống" ControlToValidate="TextPass" Text="(*)" CssClass="Red"></asp:RequiredFieldValidator>                                                
                    </td>
                </tr>
                <tr>
                    <td style="color:blue;font-size:15px; text-align: right">
                        Xác Nhận
                    </td>
                    <td>
                        <asp:TextBox ID="TextNhapLai" CssClass="form-control"  Width="100%" runat="server" TextMode="Password"></asp:TextBox>
                        <asp:CompareValidator ID="CompareValidator1" runat="server" ErrorMessage="Xác nhận mật khẩu không chính xác" ControlToCompare="TextNhapLai" ControlToValidate="TextPass" Text="(*)" CssClass="Red"></asp:CompareValidator>
                    </td>
                </tr>
                <tr>
                    <td style="text-align: center;" colspan ="2">
                        <asp:Button ID="dangky" runat="server" CssClass="btn btn-success" Text="Đăng Ký" Font-Bold="True" ForeColor="#000066" Width="90%" OnClick ="dangky_Click"/>
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                        <asp:Label ID="TextDebug" runat="server" Text="" ForeColor="Red"></asp:Label>
                        <asp:ValidationSummary ID="ValidationSummary1" runat="server" HeaderText="Lỗi" CssClass="Red" />
                    </td>
                </tr>
            </table>
        </div>
    </form>
</body>
</html>
