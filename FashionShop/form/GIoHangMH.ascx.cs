﻿using System;
using System.Data;
using FashionShop.AllClass;

namespace FashionShop.form
{
    public partial class GIoHangMH : System.Web.UI.UserControl
    {
        Connect conn;
        public int IDCTDH = 0;
        public bool Huy;
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                conn = new Connect();
                conn.Open();
                Result res = conn.GetData("SELECT Avatar, TenMH, GiaBan, SoLuong, MucGiamGia , Loai, DoiTuong,  ThuongHieu, Nam, chitietdh.MaMH FROM chitietdh left join khuyenmai on chitietdh.MaKM = khuyenmai.MaKM inner join mathang on mathang.MaMH = chitietdh.MaMH inner join thongtinmh on mathang.MaMH = thongtinmh.MaMH where chitietdh.IDCTDH = " + IDCTDH);
                if(res.complete)
                {
                    DataTable dt = (DataTable)res.res;
                    if(dt.Rows.Count > 0)
                    {
                        DataRow r = dt.Rows[0];
                        controlImage.ImageUrl = "../" + r[0];
                        controlTMH.Text = r[1].ToString();
                        int Gia = Convert.ToInt32(r[2]),
                            SoLuong = Convert.ToInt32(r[3]);
                        int GiamGia = 0;
                        if (r[4] != DBNull.Value)
                            GiamGia = Convert.ToInt32(r[4]);
                        controlTT.Text = String.Format("{0} ({1} * {2} - {3}%)", (int)(Gia - ((float)GiamGia/100) * Gia) * SoLuong, SoLuong , Gia, GiamGia);
                        controlL.Text = r[5].ToString();
                        controlDT.Text = r[6].ToString();
                        controlTH.Text = r[7].ToString() + " năm " + r[8].ToString();
                        HyperLink.NavigateUrl = "~/form/Detailed.aspx?MaMH=" + r[9].ToString();
                        if (Huy)
                            Button.Visible = false;
                    }
                }
            }
            catch(Exception)
            { }
            finally
            {
                conn.Close();
            }
        }

        protected void Huy_Click(object sender, EventArgs e)
        {
            try
            {
                Connect conn = new Connect();
                conn.Open();
                Result res = conn.GetData("DELETE FROM chitietdh WHERE IDCTDH = " + IDCTDH);
                if (res.complete)
                {
                    Response.Redirect(Request.Url.PathAndQuery);
                }
            }
            catch (Exception)
            { }
            finally
            {
                conn.Close();
            }
        }
    }
}