﻿ <%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Login.aspx.cs" Inherits="FashionShop.form.Login" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Login</title>
    <link href="../css/bootstrap.css" rel="stylesheet" />
    <link href="../css/Login.css" rel="stylesheet" />
</head>
<body>
    <form id="form1" runat="server" method="post">
        <div class="noidung">
            <table style="width:100%; margin-top:15px">
                <tr>
                    <td style="text-align:center" colspan="2">
                       <a href="home.aspx">
                           <asp:Image ID="Image1" CssClass="icon-bar" Width="140px" Height="140px" ImageUrl="../pic/ab1.png" runat="server" />
                        </a>  
                    </td>
                </tr>
                <tr>
                    <td>
                        Account
                    </td>
                    <td  style="padding:8px 25px 8px 25px">
                        <asp:TextBox ID="TextAcc" CssClass="form-control" BorderColor="#1d89e4" BackColor="#ffffcc" Height="40px" Width="100%" runat="server" MaxLength="30"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td>
                        Password
                    </td>
                    <td  style="padding:8px 25px 8px 25px">
                        <asp:TextBox ID="TextPass" CssClass="form-control" BorderColor="#1d89e4" BackColor="#ffffcc" Height="40px" Width="100%" runat="server" TextMode="Password" MaxLength="30"></asp:TextBox>
                        
                    </td>
                </tr>
                <tr>
                    <td  style="padding:8px 25px 8px 25px" colspan="2">
                         <asp:Button ID="ButtonLogin" CssClass="form-control" Height="40px" Width="100%" ForeColor="white" BackColor="#1d89e4" runat="server" Text="Login" />
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:Label ID="TextDebug" runat="server" Text="" ForeColor="#CC0000"></asp:Label>
                    </td>
                </tr>
            </table>
        </div>
    </form>
</body>
</html>
