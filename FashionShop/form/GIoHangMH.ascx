﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="GIoHangMH.ascx.cs" Inherits="FashionShop.form.GIoHangMH" %>
<div style="width:95%; margin: 5px auto auto auto; border:1px groove black" class="panel">
    <table  class="table table-condensed">
        <tr>
            <td rowspan="6">
                <asp:Image ID="controlImage" runat="server" Width="200px" Height="200px"/>
            </td>
            <td>
                Tên Mặt Hàng
            </td>
            <td>
                <asp:Label ID="controlTMH" runat="server" Text="Label"></asp:Label>
            </td>
        </tr>
        <tr>
            <td>
                Thành tiền
            </td>
            <td>
                <asp:Label ID="controlTT" runat="server" Text="Label"></asp:Label>
            </td>
        </tr>
        <tr>
            <td>
                Loại
            </td>
            <td>
                <asp:Label ID="controlL" runat="server" Text="Label"></asp:Label>
            </td>
        </tr>
        <tr>
            <td>
                Đối Tượng
            </td>
            <td>
                <asp:Label ID="controlDT" runat="server" Text="Label"></asp:Label>
            </td>
        </tr>
        <tr>
            <td>
                Thương Hiệu
            </td>
            <td>
                <asp:Label ID="controlTH" runat="server" Text="Label"></asp:Label>
            </td>
        </tr>
        <tr>
            <td>
                <asp:Button ID="Button" runat="server" Text="Hủy Giao Dịch" OnClick="Huy_Click"/>
            </td>
            <td>
                <asp:HyperLink ID="HyperLink" runat="server">Xem Chi Tiết</asp:HyperLink>
            </td>
        </tr>
    </table>
</div>