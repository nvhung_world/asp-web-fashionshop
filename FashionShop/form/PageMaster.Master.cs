﻿using System;
using System.Data;
using FashionShop.AllClass;
namespace FashionShop.form
{
    public partial class PageMaster : System.Web.UI.MasterPage
    {
        Connect conn;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["User"] != null)
            {
                LabelTND.Text = Session["HoTen"].ToString();
                LabelTND.Visible = HyperLinkTCN.Visible = HyperLinkDX.Visible = true;
                HyperLinkDN.Visible = HyperLinkDK.Visible = false;
            }
            if(TextSearch.Text != "")
            {
                Response.Redirect(GetURL("home.aspx?TenMH=" + TextSearch.Text));
            }
            try
            {
                conn = new Connect();
                conn.Open();
                LapKhuyenMai();
                LapThuongHieu();
                LapDoiTuong();
                LapLoai();
                LapNam();
                conn.Close();
            }
            catch(Exception)
            { }
        }
        private void LapKhuyenMai()
        {
            LiteralKM.Text = "";
            Result res = conn.GetData("SELECT distinct MucGiamGia FROM khuyenmai where NgayBD >= CURDATE() and NgayKT <= CURDATE()");
            if (res.complete)
            {
                DataTable dt = (DataTable)res.res;
                foreach(DataRow r in dt.Rows)
                {
                    LiteralKM.Text += GetLink("home.aspx?KhuyenMai=" + r[0].ToString(), r[0].ToString() + " %");
                }
            }
        }
        private void LapThuongHieu()
        {
            LiteralTH.Text = "";
            Result res = conn.GetData("SELECT distinct ThuongHieu FROM thongtinmh");
            if (res.complete)
            {
                DataTable dt = (DataTable)res.res;
                foreach (DataRow r in dt.Rows)
                {
                    LiteralTH.Text += GetLink("home.aspx?ThuongHieu=" + r[0].ToString(), r[0].ToString());
                }
            }
        }
        private void LapDoiTuong()
        {
            LiteralDT.Text = "";
            Result res = conn.GetData("SELECT distinct DoiTuong FROM thongtinmh");
            if (res.complete)
            {
                DataTable dt = (DataTable)res.res;
                foreach (DataRow r in dt.Rows)
                {
                    LiteralDT.Text += GetLink("home.aspx?DoiTuong=" + r[0].ToString(), r[0].ToString());
                }
            }
        }
        private void LapNam()
        {
            LiteralN.Text = "";
            Result res = conn.GetData("SELECT distinct Nam FROM thongtinmh");
            if (res.complete)
            {
                DataTable dt = (DataTable)res.res;
                foreach (DataRow r in dt.Rows)
                {
                    LiteralN.Text += GetLink("home.aspx?Nam=" + r[0].ToString(), r[0].ToString());
                }
            }
        }
        private void LapLoai()
        {
            LiteralL.Text = "";
            Result res = conn.GetData("SELECT distinct Loai FROM mathang");
            if (res.complete)
            {
                DataTable dt = (DataTable)res.res;
                foreach (DataRow r in dt.Rows)
                {
                    LiteralL.Text += GetLink("home.aspx?Loai=" + r[0].ToString(), r[0].ToString());
                }
            }
        }
        private string GetLink(string url, string text)
        {
            return "<a href=" + GetURL(url) + ">" + text + "</a>";
        }
        private string GetURL(string url)
        {
            while (url.IndexOf(' ') > -1)
            {
                int start = url.IndexOf(' ');
                url = url.Remove(start, 1).Insert(start, "%20");
            }
            return url;
        }
    }
}