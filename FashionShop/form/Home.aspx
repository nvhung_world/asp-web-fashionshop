﻿<%@ Page Title="" Language="C#" MasterPageFile="~/form/PageMaster.Master" AutoEventWireup="true" CodeBehind="Home.aspx.cs" Inherits="FashionShop.form.Home" %>

<%@ Register Src="~/form/IconMH.ascx" TagPrefix="uc1" TagName="IconMH" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
     <h3 style="margin:300px auto auto 50px"><strong style="color:red">Hot</strong> Products</h3>
    <div>
        <ul class="hot">
            <li>
                <div class="row">
                    <div class="col-md-3 col-sm-6">
                        <uc1:IconMH runat="server" ID="IconMH0"  />
                    </div>
                    <div class="col-md-3 col-sm-6">
                        <uc1:IconMH runat="server" ID="IconMH1" />
                    </div>
                    <div class="col-md-3 col-sm-6">
                        <uc1:IconMH runat="server" ID="IconMH2" />
                    </div>
                    <div class="col-md-3 col-sm-6">
                        <uc1:IconMH runat="server" ID="IconMH3" />
                    </div>
                </div>
            </li>
            <li>
                <div class="row">
                    <div class="col-md-3 col-sm-6">
                        <uc1:IconMH runat="server" ID="IconMH4" />
                    </div>
                    <div class="col-md-3 col-sm-6">
                        <uc1:IconMH runat="server" ID="IconMH5" />
                    </div>
                    <div class="col-md-3 col-sm-6">
                        <uc1:IconMH runat="server" ID="IconMH6" />
                    </div>
                    <div class="col-md-3 col-sm-6">
                        <uc1:IconMH runat="server" ID="IconMH7" />
                   </div>
                </div>
            </li>
        </ul>
    </div>
    <div>
        <table class="table table-hover" style="width: auto; margin-left: auto; margin-right: auto">
            <tr>
                <asp:Literal ID="TextTrang" runat="server"></asp:Literal>
            </tr>
        </table>
    </div>
</asp:Content>
