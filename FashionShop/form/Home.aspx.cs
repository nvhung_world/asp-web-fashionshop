﻿using System;
using System.Data;
using FashionShop.AllClass;

namespace FashionShop.form
{
    public partial class Home : System.Web.UI.Page
    {
        Connect conn;
        int page = 0;
        string where = "",
            from = "  from ((khuyenmai inner join chitietkm on khuyenmai.MaKM = chitietkm.MaKM) right join mathang on chitietkm.MaMH = mathang.MaMH  and NgayBD < curdate() and NgayKT> curdate()) inner join thongtinmh on mathang.MaMH = thongtinmh.MaMH ";
        IconMH[] MHlist;
        protected void Page_Load(object sender, EventArgs e)
        {
            MHlist = new IconMH[] { IconMH0, IconMH1, IconMH2, IconMH3, IconMH4, IconMH5, IconMH6, IconMH7 };
            Session["OldURL"] = Request.Url.PathAndQuery;
            if (Request.QueryString["TenMH"] != null)
                where += " where upper(TenMH) like upper('%" + Request.QueryString["TenMH"] + "%')";
            else if (Request.QueryString["KhuyenMai"] != null)
            {
                where += " where MucGiamGia = " + Request.QueryString["KhuyenMai"] + "";
            }
            else if (Request.QueryString["ThuongHieu"] != null)
                where += " where ThuongHieu like '" + Request.QueryString["ThuongHieu"] + "'";
            else if (Request.QueryString["DoiTuong"] != null)
                where += " where DoiTuong like '" + Request.QueryString["DoiTuong"] + "'";
            else if (Request.QueryString["Nam"] != null)
                where += " where Nam = " + Request.QueryString["Nam"] + "";
            else if (Request.QueryString["Loai"] != null)
                where += " where Loai like '" + Request.QueryString["Loai"] + "'";
            try
            {
                conn = new Connect();
                conn.Open();
                LapTrang();
                LapMatHang();
                conn.Close();
            }
            catch(Exception)
            { }
        }
        protected void LapMatHang()
        {
            Result res = conn.GetData("SELECT TenMH, GiaBan, MucGiamGia, ThuongHieu, LuotXem, Avatar, mathang.MaMH " + from + where + " limit " + page * 8 + ", 8");
            if (res.complete)
            {
                DataTable dt = (DataTable)res.res;
                for(int i= 0; i< 8; i++)
                {
                    if (i >= dt.Rows.Count)
                        MHlist[i].Visible = false;
                    else
                    {
                        DataRow r = dt.Rows[i];
                        MHlist[i].TenMH = r[0].ToString();
                        MHlist[i].GiaTien = r[1].ToString();
                        MHlist[i].KhuyenMai = r[2].ToString();
                        MHlist[i].ThuongHieu = r[3].ToString();
                        MHlist[i].LuotXem = r[4].ToString();
                        MHlist[i].Avatar = r[5].ToString();
                        MHlist[i].MaMH = r[6].ToString();
                    }
                }
            }
        }
        protected void LapTrang()
        {
            if (Request.QueryString["page"] != null)
                page = int.Parse(Request.QueryString["page"]);
            int maxpage = 0;
            Result res = conn.GetData("SELECT count(mathang.MaMH) " + from + where);
            if (res.complete)
            {
                DataTable dt = (DataTable)res.res;
                int SoMH = int.Parse(dt.Rows[0][0].ToString());
                maxpage = SoMH / 8 + (SoMH % 8 == 0 ? 0 : 1) - 1;
                if (maxpage < 0)
                    maxpage = 0;
            }
            if (page > maxpage)
                page = maxpage;
            if (page < 0)
                page = 0;
            int minPageText = (page - 5 > 0 ? page - 5: 0), 
                MaxPageText = (page + 5 < maxpage ? page + 5 : maxpage);
            for (int i = minPageText; i <= MaxPageText;i++)
            {
                if (page != i)
                {
                    string url = Request.Url.PathAndQuery;
                    if (url.LastIndexOf("page") > -1)
                    {
                        url = url.Remove(url.LastIndexOf("page"));
                        url += "page=" + i;
                    }
                    else
                    {
                        if(url.IndexOf('?')>-1)
                            url += "&page=" + i;
                        else
                            url += "?page=" + i;
                    }
                    TextTrang.Text += "<td>" + GetLink(url, i.ToString()) + "</td>";
                }
                else
                    TextTrang.Text += "<td>" + i + "</td>";
            }
        }
        private string GetLink(string url, string text)
        {
            return "<a href=" + GetURL(url) + ">" + text + "</a>";
        }
        private string GetURL(string url)
        {
            while (url.IndexOf(' ') > -1)
            {
                int start = url.IndexOf(' ');
                url = url.Remove(start, 1).Insert(start, "%20");
            }
            return url;
        }
    }
}