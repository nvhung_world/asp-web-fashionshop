﻿using System;
using System.Data;
using FashionShop.AllClass;
namespace FashionShop.form
{
    public partial class Login : System.Web.UI.Page
    {
        Connect conn;
        protected void Page_Load(object sender, EventArgs e)
        {
            if(Session["User"] != null)
                Response.Redirect("home.aspx");
            if (Request.Form["TextPass"] != null && Request.Form["TextAcc"] != null)
            {
                conn = new Connect();
                conn.Open();
                string acc = TextAcc.Text,
                    pass = TextPass.Text;
                Result res = conn.GetData("select taikhoan.MaTK, Quyen, concat(Ho, ' ', Ten) from taikhoan, thongtintk where taikhoan.MaTK = thongtintk.MaTK and upper(TaiKhoan) = upper(N'" + acc + "') and MatKhau = N'" + pass + "'");
                if (res.complete)
                {
                    DataTable dt = (DataTable)res.res;
                    if (dt.Rows.Count > 0)
                    {
                        Session["User"] = acc;
                        Session["MaTK"] = dt.Rows[0][0].ToString();
                        Session["Quyen"] = dt.Rows[0][1].ToString();
                        Session["HoTen"] = dt.Rows[0][2].ToString();
                        TextDebug.Text = "";
                        res = conn.ExecuteNonQuery("UPDATE taikhoan SET LastLogin = '"+ GetNow()+"' WHERE MaTK = " + dt.Rows[0][0].ToString());
                        LayDonHang();
                        if(Session["oldURL"] == null)
                            Response.Redirect("Home.aspx");
                        else
                            Response.Redirect(Session["oldURL"].ToString());
                    }
                    else
                    {
                        TextDebug.Text = "Sai thông tin đăng nhập";
                    }
                }
                else
                {
                    TextDebug.Text = "Lỗi: không xác định, thử lại";
                }
                conn.Close();
            }
        }
        protected void LayDonHang()
        {
            Result res = conn.GetData("SELECT MaDH, TrangThai, NgayBan, NoiGiao FROM fashionshop.donhang where MaTK = " + Session["MaTK"].ToString());
            if (res.complete)
            {
                DataTable dt = (DataTable)res.res;
                if (dt.Rows.Count > 0)
                {
                    Session["MaDH"] = dt.Rows[0][0].ToString();
                    Session["TrangThai"] = dt.Rows[0][1].ToString();
                    Session["NgayBan"] = dt.Rows[0][1].ToString();
                    Session["NoiGiao"] = dt.Rows[0][1].ToString();
                }
                else
                {
                    res = conn.ExecuteNonQuery("INSERT INTO donhang(MaTK, NgayBan)VALUES("+ Session["MaTK"].ToString() + ", '"+  GetNow() +"');");
                    LayDonHang();
                }
            }
        }
        protected string GetNow()
        {
            return "" + DateTime.Now.Year + "-" + DateTime.Now.Month + "-" + DateTime.Now.Day + "";
        }
    }
}