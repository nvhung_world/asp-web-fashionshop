﻿using System;
using System.Data;
using FashionShop.AllClass;

namespace FashionShop.form
{
    public partial class Detailed : System.Web.UI.Page
    {
        Connect conn;
        string MaMH;
        int LuongMua;
        string IDCTDH;
        protected void Page_Load(object sender, EventArgs e)
        {
            Session["oldURL"] = Request.Url.PathAndQuery;
            KiemTraDangNhap();
            if (Request.QueryString["MaMH"] != null)
            {
                MaMH = Request.QueryString["MaMH"];
                if (IsPostBack)
                    return;
                try
                {
                    conn = new Connect();
                    conn.Open();
                    string query = "SELECT TenMH, GiaBan, MucGiamGia, ThuongHieu, LuotXem, Avatar, MoTaNgan, Nam, Loai, DoiTuong   from ((khuyenmai inner join chitietkm on khuyenmai.MaKM = chitietkm.MaKM) right join mathang on chitietkm.MaMH = mathang.MaMH  and NgayBD < curdate() and NgayKT> curdate()) inner join thongtinmh on mathang.MaMH = thongtinmh.MaMH where mathang.MaMH = " + MaMH;
                    Result res = conn.GetData(query);
                    if(res.complete)
                    {
                        DataTable dt = (DataTable)res.res;
                        if(dt.Rows.Count > 0)
                        {
                            DataRow r = dt.Rows[0];
                            TenMH.Text = r[0].ToString();
                            int GT = 0;
                            float KM = 0;
                            GiaTien.Text = r[1].ToString();
                            GT = int.Parse(GiaTien.Text);
                            if (r[2] != DBNull.Value)
                            {
                                KhuyenMai.Text = r[2].ToString() + " %";
                                KM = float.Parse(r[2].ToString()) / 100;
                            }
                            ThanhTien.Text = (GT - (int)(KM * GT)).ToString();
                            ThuongHieu.Text = r[3].ToString();
                            
                            Avatar.ImageUrl = "../" + r[5].ToString();
                            MoTa.Text = r[6].ToString();
                            Nam.Text = r[7].ToString();
                            Loai.Text = r[8].ToString();
                            DoiTuong.Text = r[9].ToString();
                            //..........................................
                            int LX = int.Parse(r[4].ToString()) + 1;
                            TangLuotXem(LX);
                            LuotXem.Text = LX.ToString();
                            //..........................................
                            LapLuongMua();
                            MuaLabel.Text = LuongMua.ToString();
                            return;
                        }
                    }
                    conn.Close();
                }
                catch(Exception)
                { }
            }
            TTSP.Text = "Không tìm thấy thông tin sản phẩm";
        }
        protected void KiemTraDangNhap()
        {
            if (Session["MaDH"] != null)
            {
                MuaDN.Visible = false;
                MuaLabel.Visible
                    = MuaLabel2.Visible
                    = MuaCong.Visible
                    = MuaTru.Visible = true;
            }
            else
            {
                MuaDN.Visible = true;
                MuaLabel.Visible
                    = MuaLabel2.Visible
                    = MuaCong.Visible
                    = MuaTru.Visible = false;
            }
        }
        protected void TangLuotXem(int LuotXem)
        {
            try
            {
                conn.ExecuteNonQuery("update thongtinmh set LuotXem=" + LuotXem + " where MaMH=" + MaMH);
            }
            catch (Exception) { }
        }
        protected void MuaTru_Click(object sender, EventArgs e)
        {
            try
            {
                if (MaMH == null)
                    return;
                conn = new Connect();
                conn.Open();
                int TonKho = 0;
                Result r = conn.GetData("SELECT TonKho FROM mathang where MaMH =" + MaMH);
                if (r.complete)
                {
                    DataRow dr = ((DataTable)r.res).Rows[0];
                    TonKho = Convert.ToInt32(dr[0]);
                }
                LapLuongMua();
                Result res = conn.ExecuteNonQuery("UPDATE chitietdh SET SoLuong = "+(LuongMua - 1)+" WHERE IDCTDH =" + IDCTDH);
                if(res.complete)
                {
                    conn.ExecuteNonQuery("UPDATE mathang SET TonKho = " + (TonKho + 1) + " WHERE MaMH =" + MaMH);
                    LuongMua--;
                    MuaLabel.Text = LuongMua.ToString();
                }
                conn.Close();

            }catch(Exception)
            { }
        }
        protected void MuaCong_Click(object sender, EventArgs e)
        {
            try
            {
                if (MaMH == null)
                    return;
                conn = new Connect();
                conn.Open();
                int TonKho = 0;
                Result r = conn.GetData("SELECT TonKho FROM mathang where MaMH =" + MaMH);
                if(r.complete)
                {
                    DataRow dr = ((DataTable)r.res).Rows[0];
                    TonKho = Convert.ToInt32(dr[0]);
                    if (TonKho <= 0)
                        return;
                }
                LapLuongMua();
                string query = "UPDATE chitietdh SET SoLuong = " + (LuongMua + 1) + " WHERE IDCTDH =" + IDCTDH;
                Result res = conn.ExecuteNonQuery(query);
                if (res.complete)
                {
                    conn.ExecuteNonQuery("UPDATE mathang SET TonKho = "+(TonKho -1)+" WHERE MaMH =" + MaMH);
                    LuongMua++;
                    MuaLabel.Text = LuongMua.ToString();
                }
                conn.Close();
            }
            catch (Exception)
            { }
        }
        protected void LapLuongMua()
        {
            if (Session["MaDH"] == null)
                return;
            IDCTDH = getCTDH();
            Result res = conn.GetData("SELECT SoLuong FROM chitietdh where IDCTDH = " + IDCTDH);
            if (res.complete)
            {
                DataTable dt = (DataTable)res.res;
                if (dt.Rows.Count > 0)
                {
                    LuongMua = Convert.ToInt32(dt.Rows[0][0]);
                }
            }
        }
        protected string getCTDH()
        {
            Result res =  conn.GetData("SELECT IDCTDH FROM chitietdh where MaDH = "+ Session["MaDH"] +" and MaMH = " + MaMH);
            if (res.complete)
            {
                DataTable dt = (DataTable)res.res;
                if (dt.Rows.Count > 0)
                {
                    return dt.Rows[0][0].ToString();
                }
                else
                {
                    string query = string.Format("INSERT INTO chitietdh(MaDH,MaMH,SoLuong,MaKM)VALUES({0},{1},0,(select Khuyenmai.MaKM from Khuyenmai, chitietkm where NgayBD < curdate() and NgayKT >curdate() and khuyenmai.MaKM = chitietkm.MaKM and chitietkm.MaMH = {2} limit 0, 1));", Session["MaDH"], MaMH, MaMH);
                    conn.ExecuteNonQuery(query);
                    return getCTDH();
                }
            }
            else
                return getCTDH();

        }
    }
}