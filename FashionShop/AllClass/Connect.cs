﻿using System;
using System.Web.Configuration;
using System.Data;
using MySql.Data.MySqlClient;

namespace FashionShop.AllClass
{
    public class Connect
    {
        public string connectString = "server=localhost;user=root;database=fashionshop;port=3307;password=hungit;";
        MySqlConnection conn;
        public void Open()
        {
            if (conn == null)
                conn = new MySqlConnection(connectString);
            if (conn.State == ConnectionState.Closed)
                conn.Open();
        }
        public void Close()
        {
            if (conn != null)
            {
                conn.Close();
                conn.Dispose();
            }
        }
        public Result GetData(string selectQuery)
        {
            Result res;
            try
            {
                MySqlDataAdapter da = new MySqlDataAdapter(selectQuery, conn);
                DataTable dt = new DataTable();
                da.Fill(dt);
                res = new Result(true, dt);
            }
            catch(Exception ex)
            {
                res = new Result(false, null, ex.Message);
            }
            return res;
        }
        public Result ExecuteScalar(string query)
        {
            Result res;
            try
            {
                MySqlCommand cmd = new MySqlCommand(query, conn);
                object result = cmd.ExecuteScalar();
                if (result != null)
                    res = new Result(true, Convert.ToInt32(result));
                else
                    res = new Result(true, -1);
            }
            catch (Exception ex)
            {
                res = new Result(false, null, ex.Message);
            }
            return res;
        }
        public Result ExecuteNonQuery(string query)
        {
            Result res;
            try
            {
                MySqlCommand cmd = new MySqlCommand(query, conn);
                object result = cmd.ExecuteNonQuery();
                if (result != null)
                    res = new Result(true, Convert.ToInt32(result));
                else
                    res = new Result(true, -1);
            }
            catch (Exception ex)
            {
                res = new Result(false, null, ex.Message);
            }
            return res;
        }
    }
}