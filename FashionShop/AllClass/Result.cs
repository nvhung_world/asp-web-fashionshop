﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FashionShop.AllClass
{
    public class Result
    {
        public object res;
        public string Error = null;
        public bool complete;
        public Result(bool complete = true, object res = null, string Error = null)
        {
            this.complete = complete;
            this.res = res;
            this.Error = Error;
        }
    }
}